﻿using System;
using System.ComponentModel;
using Clarika.Services;

namespace Clarika.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public bool IsBusy { get; set; }
        public IManager Api { get; set; }

        public BaseViewModel()
        {
            Api = new Manager();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
