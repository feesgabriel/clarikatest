﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Clarika.Models;
using Xamarin.Forms;

namespace Clarika.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {
        public ICommand GetProductsCommand { get; set; }
        public List<Product> Products { get; set; }
        private MainPage mainPage;

        public MainPageViewModel(MainPage page)
        {
            GetProductsCommand = new Command(async () => await GetProducts());
            GetProductsCommand.Execute(null);
            mainPage = page;
           
        }

        async Task GetProducts()
        {
            IsBusy = true;
            Products = await Api.GetProducts();
            IsBusy = false;

           

        }
            
        public void InStock(string textId){

            var product = GetProductID(textId);

            if (product != null)
            {//id in products
              
                Device.BeginInvokeOnMainThread(async () => {
                    await ShowMessage( "Description: " + product.Description + "\n Price: " + product.Currency+product.Price, product.Name, "Aceptar", () =>
                    {
                        mainPage.EnableScan();
                    });
                });
            }
            else {
                Device.BeginInvokeOnMainThread(async () => {
                    await ShowMessage("Error", "Producto no Registrado", "Aceptar", () =>
                    {
                        mainPage.EnableScan();
                    });
                });
            }
        }

        private Product GetProductID(string id) {
            //se buscaria el id en los productos, para el fin del ejecicio solo lo simulo
            Product p;

            if (RandomNumber(0,2).Equals(0)) {
                //Producto no registrado
                p = null;
            }
            else {
                //Producto encontrado
                p = Products.ToArray()[RandomNumber(0,10)];
            }

            return p;

        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            var r = random.Next(min, max);
            return r;
        }

        public async Task ShowMessage(string message,
        string title,
        string buttonText,
        Action afterHideCallback)
        {
            await mainPage.DisplayAlert(
                title,
                message,
                buttonText);

            afterHideCallback?.Invoke();
        }


    }
}
