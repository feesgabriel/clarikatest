﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Models;
using Clarika.Services;
using Newtonsoft.Json;

namespace Clarika.Services
{
    public class Manager : IManager
    { 
        public async Task<List<Product>> GetProducts()
        {
            await Task.Delay(1000);

            var products = new List<Product>();
            products.Add(new Product() { Id = 12345670, Name = "Coca Cola", Description = "Description 00", Currency = "ARS ", Price = 80.0 });
            products.Add(new Product() { Id = 12345671, Name = "Smart TV Samsung", Description = "Description 01", Currency = "USD ", Price = 100.0 });
            products.Add(new Product() { Id = 12345672, Name = "Silla Oficina", Description = "Description 02", Currency = "ARS ", Price = 800.0 });
            products.Add(new Product() { Id = 12345673, Name = "Espejo antiguo", Description = "Description 03", Currency = "USD ", Price = 150.0 });
            products.Add(new Product() { Id = 12345674, Name = "Teclado Genius", Description = "Description 04", Currency = "ARS ", Price = 200.0 });
            products.Add(new Product() { Id = 12345675, Name = "Mouse Inalambrico", Description = "Description 05", Currency = "USD ", Price = 20.0 });
            products.Add(new Product() { Id = 12345676, Name = "Parlante Portatil", Description = "Description 06", Currency = "USD ", Price = 50.0 });
            products.Add(new Product() { Id = 12345677, Name = "Teclado Yamaha", Description = "Description 07", Currency = "ARS ", Price = 2000.0 });
            products.Add(new Product() { Id = 12345678, Name = "Cable HDMI", Description = "Description 08", Currency = "ARS ", Price = 50.0 });
            products.Add(new Product() { Id = 12345679, Name = "Auriculares", Description = "Description 09", Currency = "ARS ", Price = 550.0 });


            /*var stringResponse = "[{json}]";
             var products = await Task.Run(() => JsonConvert.DeserializeObject<List<Product>>(stringResponse));*/

            return products;
        }
    }
}