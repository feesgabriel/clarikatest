﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Clarika.Models;

namespace Clarika.Services
{
    public interface IManager
    {
        Task<List<Product>> GetProducts();
    }
}