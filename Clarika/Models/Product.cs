﻿using System;
using Newtonsoft.Json;

namespace Clarika.Models
{
    public class Product
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("Currency")]
        public string Currency { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }
    }
}
