﻿using System;
using System.Threading.Tasks;
using Clarika.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing;


namespace Clarika
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            BindingContext = new MainPageViewModel(this);

        }

        private async void ZXingScannerView_OnOnScanResult(Result result)
        {
            // Stop analysis 
            zxing.IsAnalyzing = false;
            //.3 remove leading zeros
            ((MainPageViewModel)BindingContext).InStock(result.Text.TrimStart('0'));
            await Task.Delay(1000);

        }

        async void OnButtonClicked(object sender, EventArgs args)
        {
            if (flashOff)
                await TurnonFlash();
            else
                await TurnoffFlash();
        }

        async Task TurnonFlash()
        {
            try
            {
                // Turn On
                await Flashlight.TurnOnAsync();
                // Turn Off
                //await Flashlight.TurnOffAsync();
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                var x = fnsEx.Message;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                var x = pEx.Message;
            }
            catch (Exception ex)
            {
                // Unable to turn on/off flashlight
                var x = ex.Message;
            }
        }
        private bool flashOff = true;

        async Task TurnoffFlash()
        {
            try
            {
                // Turn On
                await Flashlight.TurnOffAsync();
                // Turn Off
                //await Flashlight.TurnOffAsync();
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                var x = fnsEx.Message;
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                var x = pEx.Message;
            }
            catch (Exception ex)
            {
                // Unable to turn on/off flashlight
                var x = ex.Message;
            }
        }

        public async Task ShowMessage(string message,
           string title,
           string buttonText,
           Action afterHideCallback)
        {
            await DisplayAlert(
                title,
                message,
                buttonText);

            afterHideCallback?.Invoke();
        }

        public void EnableScan() {

            zxing.IsScanning = true;
            zxing.IsAnalyzing = true;
        }

        private void Overlay_OnFlashButtonClicked(Button sender, EventArgs e)
        {
            zxing.IsTorchOn = !zxing.IsTorchOn;

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            EnableScan();

        }

        protected override void OnDisappearing()
        {
            zxing.IsScanning = false;

            base.OnDisappearing();
        }


    }
}
